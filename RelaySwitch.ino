#include <ESP8266WiFi.h>

#include <PubSubClient.h>

#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic


const char* mqttServer = "10.0.0.40";
int mqttPort = 1883;

WiFiClient espClient;
PubSubClient client(espClient);

const char* inTopic = "test/out";
const char* outTopic = "test/out";

uint8_t relayPin = D0;
int relayState = 0;

unsigned long delayStart = 0; // the time the delay started
unsigned long totalDelay;
bool delayRunning = false; // true if still waiting for delay to finish

// ===============================================================
// Program setup, connect to WIFI and configure MQTT client
// ===============================================================
void setup() {

  Serial.begin(115200);

  pinMode(relayPin, OUTPUT);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;

  //tries to connect to last known settings
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP" with password "password"
  //and goes into a blocking loop awaiting configuration
  wifiManager.autoConnect("AutoConnectAP");

  Serial.println("Connected to Wifi");

  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);

}

// ===============================================================
// Main program loop
// ===============================================================
void loop() {

  // MQTT connection
  if (!client.connected()) {
    reconnect();
  }

  // Check if there is a active running timer
  if (delayRunning && ((millis() - delayStart) >= totalDelay)) {
    delayRunning = false; // prevent this code being run more then once
    digitalWrite(relayPin, LOW); // turn led off
    // Update relay state
    relayState = 0;
      
    Serial.println("End of delay - Turn LED Off");

    // Reset total delay
    totalDelay = 0;
  }

  client.loop();

}

// ===============================================================
// Handle connection to MQTT
// ===============================================================
void reconnect() {

  
  while (!client.connected()) {

    Serial.print("Attempting MQTT connection...");

    // If you do not want to use a username and password, change next line to
    if (client.connect("ESP8266Client")) {
      //if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
      
      Serial.println("Connected to MQTT");
      // Once connected, publish an announcement...
      client.publish(inTopic, "ESP8266 booted");
      // ... and resubscribe
      client.subscribe(outTopic);

    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// ===============================================================
// Code to be run when a MQTT message is recieved
// ===============================================================
void callback(char* topic, byte* payload, unsigned int length) {

  int status;
  int i = 0;

  char response[64];
  char input[length + 1];
  String duration = "";

  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("]\n");

  // Convert input byte array to char array for processing
  for (i = 0; i < length; i++) {
    input[i] = (char)payload[i];
  }
  input[i] = '\0';

  String messageIdString = getSubstring(input, ';', 0);
  String messageId = getSubstring(messageIdString, '=', 1);

  int msgId = messageId.toInt();

  String command = getSubstring(input, ';', 1);

  if (command.startsWith("result")) {
    // If command starts with result, do nothing...

  } else if (command.startsWith("status")) {
    Serial.println();
    Serial.println(">>>Status command recieved<<<");

    snprintf(response, 32, "resId=%d;result=1;message=%d\0", msgId, relayState);
    
    Serial.println(response);
    client.publish(inTopic, response);

  } else if (command.startsWith("toggle")) {
    Serial.println(">>>Toggle command recieved<<<");

    // Get toggle param
    String toggleParam = getSubstring(command, '=', 1);

    if (toggleParam.length() == 0) {
      Serial.println("ERROR: No toggle state recieved.");
      snprintf(response, 55, "resId=%d;result=0;message=%s\0", msgId, "No toggle state recieved.");
      client.publish(inTopic, response);
    }

    // Get optional duration if present
    String durationParam = getSubstring(input, ';', 2);
    
    if (durationParam.length() > 0) {
      duration = getSubstring(durationParam, '=', 1);
      Serial.println("Duration=");
      Serial.print(duration);
      Serial.print( "seconds");
      
      totalDelay = (duration.toInt())*1000; // Duration is in seconds, convert to milliseconds
      
    } else {
      totalDelay = 1200000; // Set default duration to 20 minutes. Make sure the solenoid does not stay open forever.
    }

    Serial.println();

    status = toggleParam.toInt();

    if (status == 0) {
      // Requested status is off, turn relay off

      digitalWrite(relayPin, LOW);   // Turn the LED off
      relayState = 0; // Update relay state

      Serial.println("relay_pin -> LOW");
      
      snprintf(response, 32, "resId=%d;result=1;message=%d\0", msgId, 1);
      client.publish(inTopic, response);

    } else if (status == 1 && totalDelay > 0) {
      // Duration parameter is set with status 1. Turn relay on for the duration

      digitalWrite(relayPin, HIGH);  // Turn the LED on by making the voltage HIGH
      relayState = 1; // Update relay state
      
      Serial.println("relay_pin -> HIGH");
      snprintf(response, 32, "resId=%d;result=1;message=%d\0", msgId, 1);
      client.publish(inTopic, response);

      delayStart = millis();   // Start timed delay
      delayRunning = true; // not finished yet

      Serial.println("Start delay");


    } else if (status == 1) {
      // No duration parameter, just turn relay on

      digitalWrite(relayPin, HIGH);   // Turn the LED
      relayState = 1; // Update relay state
      
      Serial.println("Turn on No duration relay_pin -> HIGH");
      snprintf(response, 32, "resId=%d;result=1;message=%d\0", msgId, 1);
      client.publish(inTopic, response);


    } else {
      //Invalid toggle parameter value. Send back error message
      
      Serial.println("ERROR: Invalid toggle parameter.");
      snprintf(response, 55, "resId=%d;result=0;message=%s\0", msgId, "Invalid toggle parameter.");
      client.publish(inTopic, response);
    }

  } else {
    
    Serial.println();
    Serial.println(">>>Unknown command recieved<<<");
    Serial.println();

    snprintf(response, 56, "resId=%d;result=0;message=%s\0", msgId, "Unknown command recieved.");
    client.publish(inTopic, response);
  }
}

// ===============================================================
// Helper function to get a delemited substring from a string
// ===============================================================
String getSubstring(String data, char separator, int index) {

  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }

  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
